import reorder from './reorder';

const withNewRoomsIds = (column, roomsIds) => ({
  id: column.id,
  title: column.title,
  roomsIds,
});

const reorderSingleDrag = ({
  entities,
  selectedRoomsIds,
  source,
  destination,
}) => {
  // moving in the same list
  if (source.droppableId === destination.droppableId) {
    const column = entities.columns[source.droppableId];
    const reordered = reorder(
      column.roomsIds,
      source.index,
      destination.index,
    );

    const updated = {
      ...entities,
      columns: {
        ...entities.columns,
        [column.id]: withNewRoomsIds(column, reordered),
      },
    };

    return {
      entities: updated,
      selectedRoomsIds,
    };
  }

  // moving to a new list
  const home = entities.columns[source.droppableId];
  const foreign = entities.columns[destination.droppableId];

  // the id of the room to be moved
  const roomId = home.roomsIds[source.index];

  // remove from home column
  const newHomeRoomsIds = [...home.roomsIds];
  newHomeRoomsIds.splice(source.index, 1);

  // add to foreign column
  const newForeignRoomsIds = [...foreign.roomsIds];
  newForeignRoomsIds.splice(destination.index, 0, roomId);

  const updated = {
    ...entities,
    columns: {
      ...entities.columns,
      [home.id]: withNewRoomsIds(home, newHomeRoomsIds),
      [foreign.id]: withNewRoomsIds(foreign, newForeignRoomsIds),
    },
  };

  return {
    entities: updated,
    selectedRoomsIds,
  };
};

export const getHomeColumn = (entities, roomId) => {
  const columnId = entities.columnOrder.find((id) => {
    const column = entities.columns[id];
    return column.roomsIds.includes(roomId);
  });

  return entities.columns[columnId];
};

const reorderMultiDrag = ({
  entities,
  selectedRoomsIds,
  source,
  destination,
}) => {
  const start = entities.columns[source.droppableId];
  const dragged = start.roomsIds[source.index];

  const insertAtIndex = (() => {
    const destinationIndexOffset = selectedRoomsIds.reduce(
      (previous, current) => {
        if (current === dragged) {
          return previous;
        }

        const final = entities.columns[destination.droppableId];
        const column = getHomeColumn(entities, current);

        if (column !== final) {
          return previous;
        }

        const index = column.roomsIds.indexOf(current);

        if (index >= destination.index) {
          return previous;
        }

        // the selected item is before the destination index
        // we need to account for this when inserting into the new location
        return previous + 1;
      },
      0,
    );

    return destination.index - destinationIndexOffset;
  })();

  // doing the ordering now as we are required to look up columns
  // and know original ordering
  const orderedSelectedRoomsIds = [...selectedRoomsIds];
  orderedSelectedRoomsIds.sort((a, b) => {
    // moving the dragged item to the top of the list
    if (a === dragged) {
      return -1;
    }
    if (b === dragged) {
      return 1;
    }

    // sorting by their natural indexes
    const columnForA = getHomeColumn(entities, a);
    const indexOfA = columnForA.roomsIds.indexOf(a);
    const columnForB = getHomeColumn(entities, b);
    const indexOfB = columnForB.roomsIds.indexOf(b);

    if (indexOfA !== indexOfB) {
      return indexOfA - indexOfB;
    }

    // sorting by their order in the selectedRoomsIds list
    return -1;
  });

  // we need to remove all of the selected rooms from their columns
  const withRemovedRooms = entities.columnOrder.reduce(
    (previous, columnId) => {
      const column = entities.columns[columnId];

      // remove the id's of the items that are selected
      const remainingRoomsIds = column.roomsIds.filter(
        (id) => !selectedRoomsIds.includes(id),
      );

      previous[column.id] = withNewRoomsIds(column, remainingRoomsIds);
      return previous;
    },
    entities.columns,
  );

  const final = withRemovedRooms[destination.droppableId];
  const withInserted = (() => {
    const base = [...final.roomsIds];
    base.splice(insertAtIndex, 0, ...orderedSelectedRoomsIds);
    return base;
  })();

  // insert all selected rooms into final column
  const withAddedRooms = {
    ...withRemovedRooms,
    [final.id]: withNewRoomsIds(final, withInserted),
  };

  const updated = {
    ...entities,
    columns: withAddedRooms,
  };

  return {
    entities: updated,
    selectedRoomsIds: orderedSelectedRoomsIds,
  };
};

export const mutliDragAwareReorder = (args) => {
  if (args.selectedRoomsIds.length > 1) {
    return reorderMultiDrag(args);
  }
  return reorderSingleDrag(args);
};

export const multiSelectTo = (
  entities,
  selectedRoomsIds,
  newRoomId,
) => {
  // Nothing already selected
  if (!selectedRoomsIds.length) {
    return [newRoomId];
  }

  const columnOfNew = getHomeColumn(entities, newRoomId);
  const indexOfNew = columnOfNew.roomsIds.indexOf(newRoomId);

  const lastSelected = selectedRoomsIds[selectedRoomsIds.length - 1];
  const columnOfLast = getHomeColumn(entities, lastSelected);
  const indexOfLast = columnOfLast.roomsIds.indexOf(lastSelected);

  // multi selecting to another column
  // select everything up to the index of the current item
  if (columnOfNew !== columnOfLast) {
    return columnOfNew.roomsIds.slice(0, indexOfNew + 1);
  }

  // multi selecting in the same column
  // need to select everything between the last index and the current index inclusive

  // nothing to do here
  if (indexOfNew === indexOfLast) {
    return null;
  }

  const isSelectingForwards = indexOfNew > indexOfLast;
  const start = isSelectingForwards ? indexOfLast : indexOfNew;
  const end = isSelectingForwards ? indexOfNew : indexOfLast;

  const inBetween = columnOfNew.roomsIds.slice(start, end + 1);

  const toAdd = inBetween.filter((roomId) => !selectedRoomsIds.includes(roomId));

  const sorted = isSelectingForwards ? toAdd : [...toAdd].reverse();

  return [...selectedRoomsIds, ...sorted];
};
