import axios from 'axios';

export default class RoomService {
  constructor(authData, onAuthError) {
    this.hsUrl = authData.hsUrl;
    this.accessToken = authData.accessToken;
    this.userId = authData.userId;
    this.userDisplayName = authData.displayName;

    this.handleError = (e) => {
      if (e.status === 401) {
        onAuthError(e);
      }
      throw e;
    };
  }

  getTagsOnRoom = async (roomId) => {
    const userId = encodeURIComponent(this.userId);
    roomId = encodeURIComponent(roomId);
    try {
      const res = await axios.get(`${this.hsUrl}/_matrix/client/r0/user/${userId}/rooms/${roomId}/tags`, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
      return res.data.tags;
    } catch (e) {
      this.handleError(e);
    }
  }

  addTagToRoom = async (roomId, tagName, order) => {
    try {
      const userId = encodeURIComponent(this.userId);
      roomId = encodeURIComponent(roomId);
      tagName = encodeURIComponent(tagName);
      await axios.put(`${this.hsUrl}/_matrix/client/r0/user/${userId}/rooms/${roomId}/tags/${tagName}`, {
        order,
      }, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
    } catch (e) {
      this.handleError(e);
    }
  }

  removeTagFromRoom = async (roomId, tagName) => {
    try {
      const userId = encodeURIComponent(this.userId);
      roomId = encodeURIComponent(roomId);
      tagName = encodeURIComponent(tagName);
      await axios.delete(`${this.hsUrl}/_matrix/client/r0/user/${userId}/rooms/${roomId}/tags/${tagName}`, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
    } catch (e) {
      this.handleError(e);
    }
  }

  getDirectChats = async () => {
    try {
      const userId = encodeURIComponent(this.userId);
      const { data } = await axios.get(`${this.hsUrl}/_matrix/client/r0/user/${userId}/account_data/m.direct`, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
      return data || [];
    } catch (e) {
      this.handleError(e);
    }
  }

  setDirectChats = async (chats) => {
    try {
      const userId = encodeURIComponent(this.userId);
      await axios.put(`${this.hsUrl}/_matrix/client/r0/user/${userId}/account_data/m.direct`, chats, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
    } catch (e) {
      this.handleError(e);
    }
  }

  getJoinedRooms = async () => {
    try {
      const { data } = await axios.get(`${this.hsUrl}/_matrix/client/r0/joined_rooms`, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });

      return data.joined_rooms || [];
    } catch (e) {
      this.handleError(e);
    }
  }

  getEffectiveJoinedMembers = async (roomId) => {
    try {
      roomId = encodeURIComponent(roomId);
      const { data } = await axios.get(`${this.hsUrl}/_matrix/client/r0/rooms/${roomId}/members`, {
        params: {
          not_membership: ['leave', 'ban'],
        },
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
      if (!data.chunk) return [];
      return data.chunk.map((e) => e.state_key).filter((u) => !!u);
    } catch (e) {
      this.handleError(e);
    }
  }

  getRooms = async () => {
    try {
      const filter = {
        presence: { types: [], limit: 0 },
        account_data: { types: [], limit: 0 },
        room: {
          ephemeral: { types: [], limit: 0 },
          state: { types: ['m.room.name', 'm.room.avatar', 'm.room.member'] },
          timeline: { types: [], limit: 0 },
          account_data: { types: [], limit: 0 },
        },
      };
      const encodedFilter = encodeURIComponent(JSON.stringify(filter));
      const { data } = await axios.get(`${this.hsUrl}/_matrix/client/r0/sync?filter=${encodedFilter}`, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
      if (!data.rooms || !data.rooms.join) return {};

      const rooms = {};
      const joinedRooms = data.rooms.join;
      for (const roomId of Object.keys(joinedRooms)) {
        const room = joinedRooms[roomId];
        if (!room.state || !room.state.events) return;

        let displayName = null;
        let avatarMxc = null;
        let memberName = null;
        let memberAvatar = null;

        for (const event of room.state.events) {
          if (!event.content) continue;

          const { content } = event;

          switch (event.type) {
            case 'm.room.name':
              displayName = content.name;
              break;
            case 'm.room.avatar':
              avatarMxc = content.url;
              break;
            case 'm.room.member':
              if (memberName) break;

              if (content.displayname !== this.userDisplayName && content.membership === 'join') {
                memberName = content.displayname;
                memberAvatar = content.avatar_url;
              }
              break;
            default: break;
          }
        }
        if (!displayName) {
          displayName = memberName || roomId;
          avatarMxc = memberAvatar;
        }
        rooms[roomId] = { displayName, avatarMxc, roomId };
      }

      return rooms;
    } catch (e) {
      this.handleError(e);
    }
  }

  getTags = async () => {
    try {
      const filter = {
        event_fields: ['content.tags'],
        presence: { types: [], limit: 0 },
        account_data: { types: [], limit: 0 },
        room: {
          ephemeral: { types: [], limit: 0 },
          state: { types: [], limit: 0 },
          timeline: { types: [], limit: 0 },
          account_data: { types: ['m.tag'] },
        },
      };
      const encodedFilter = encodeURIComponent(JSON.stringify(filter));

      const { data } = await axios.get(`${this.hsUrl}/_matrix/client/r0/sync?filter=${encodedFilter}`, {
        headers: {
          Authorization: `Bearer ${this.accessToken}`,
        },
      });
      if (!data.rooms || !data.rooms.join) return {};

      const resultingTags = {};

      const joinedRooms = data.rooms.join;
      for (const roomId of Object.keys(joinedRooms)) {
        const accountData = joinedRooms[roomId].account_data;
        if (!accountData || !accountData.events) continue;
        for (const event of accountData.events) {
          if (event.type !== 'm.tag') continue;

          const { content } = event;
          if (!content || !content.tags) continue;

          for (const tagName of Object.keys(content.tags)) {
            const tag = content.tags[tagName];
            if (!resultingTags[tagName]) resultingTags[tagName] = [];
            const order = !tag.order && tag.order !== 0 ? 1 : Number(tag.order);
            resultingTags[tagName].push({ order, roomId });
          }
        }
      }

      // Sort the tags by order
      for (const tagName of Object.keys(resultingTags)) {
        resultingTags[tagName].sort((a, b) => a.order - b.order);
      }

      return resultingTags;
    } catch (e) {
      this.handleError(e);
    }
  }
}
