import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss';
import Loader from './Loader';

const Button = ({
  loading, children, className, disabled, ...restProps
}) => (
  <button
    className={`button ${loading ? 'button--loading' : ''} ${className}`}
    disabled={disabled || loading}
    {...restProps}
  >
    {loading ? <Loader size="small" /> : children}
  </button>
);

Button.defaultProps = {
  loading: false,
  children: '',
  className: '',
};

Button.propTypes = {
  children: PropTypes.node,
  loading: PropTypes.bool,
  className: PropTypes.string,
};

export default Button;
